import java.io.*;
import java.util.*;
/**
* ClusterPair.java is a class which stores the distance between two priority queues
* It extends comparable by comparing distance.
*
* @author Brynna Mering
*/
public class ClusterPair implements Comparable<ClusterPair>{
	private Set<Integer> cluster1;
	private Set<Integer> cluster2;
	private Double distance;

	/**
	* Constructor for ClusterPair
	*/
	public ClusterPair(Set<Integer> c1, Set<Integer> c2, Double distance){
		this.distance = distance;
		this.cluster1 = c1;
		this.cluster2 = c2;
	}

	/**
	* Constructor for ClusterPair
	*/
	public ClusterPair(ClusterPair clusterPair){
		this.distance = clusterPair.getDistance();
		this.cluster1 = clusterPair.getCluster1();
		this.cluster2 = clusterPair.getCluster2();
	}

	public Double getDistance(){
		return this.distance;
	}

	public Set<Integer> getCluster1(){
		return this.cluster1;
	}

	public Set<Integer> getCluster2(){
		return this.cluster2;
	}

	/**
	* compareTo() allows this ClusterPair to be compared to another ClusterPair based on distance
	*/

	public int compareTo(ClusterPair compareClusterPair){
		if(compareClusterPair.getDistance()>this.distance){
			return -1;
		} else if(compareClusterPair.getDistance() == this.distance){
			return 0;
		} else {
			return 1;
		}
	}
}