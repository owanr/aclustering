import java.io.*;
import java.util.*;
import java.lang.*;
/*
AgglomerativeClustering, due 2/23
Written by Brynna Mering and Risako Owan
*/

public class AgglomerativeClustering {
    private static HashMap<Integer, List<Double>> idToData = new HashMap<Integer, List<Double>>();
    private static HashMap<Set<Integer>, List<Double>> clusterAssignments = new HashMap<Set<Integer>, List<Double>>();
    private static PriorityQueue<ClusterPair> distPriorityQueue = new PriorityQueue<ClusterPair>();
    private static HashMap<Integer, List<Double>> standardizedValues = new HashMap<Integer, List<Double>>();
    private static int dimensions = 0;
    private static int k;

    /*
    ** Reads data from file and adds it to idToData
    ** Also calculates dimensions (# of columns)
    */
    private static void readFile() {
        File dataFile = new File("portfoliodata.txt");
        BufferedReader reader = null;
        //Reads data from file and adds them to idToData and initial values to clusterAssignments
        try {
            reader = new BufferedReader(new FileReader(dataFile));
            reader.readLine(); //Gets rid of headers
            String line = reader.readLine();
            Integer id = 0;
            while (line != null) {
                String[] splitLine = line.split("\t");
                List<Double> studentData = new ArrayList<Double>();
                for (String data : splitLine) {
                    Double value = null;
                    //No value can be 9999.99 or an empty string
                    if (!(data.trim().equals("9999.99") || data.trim().equals(""))) {
                        value = Double.parseDouble(data);
                    }
                    studentData.add(value);
                }
                idToData.put(id, studentData);
                id++;
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                dimensions = idToData.get(0).size();//for use in calcEucDist
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
            }
        }
    }

    /*
    ** Computes the mean of all of the values in the columns
    ** Is called in standardDeviation()
    */
    public static Double[] average(){
        Integer[] numRows = new Integer[dimensions];
        Double[] averageArray = new Double[dimensions];
        Integer dataSize = (Integer)idToData.size();
        //Initialize all values in array to 0
        for (int d = 0; d < dimensions; d++){
            numRows[d] = dataSize;
            averageArray[d] = 0.0;
        }

        for (int id = 0; id < idToData.size(); id++) {
            List<Double> studentData = new ArrayList<Double>(idToData.get(id));
            for (int j = 0; j < dimensions; j++) {
                if (studentData.get(j) != null) {
                    averageArray[j] = averageArray[j] + studentData.get(j);
                } else {
                    numRows[j] = numRows[j]-1;
                }
            }
        }
        //Now averageArray contains sums for all columns
        
        for(int i = 0; i < dimensions; i++){
            averageArray[i] = averageArray[i]/numRows[i];
        }
        return averageArray;
    }

    /*
    ** Computes the standard deviation from data in idToData
    ** Calls average(), gets called in standardizeData
    */
    public static Double[] standardDeviation(){
        Double[] averageArray = average();
        Integer[] numRows = new Integer[dimensions];
        Double[] standardDeviation = new Double[dimensions];
        Integer dataSize = (Integer)idToData.size();
        
        //Initialize all values in array to 0
        for (int d = 0; d < dimensions; d++){
            numRows[d] = dataSize;
            standardDeviation[d] = 0.0;
        }

        for (int id = 0; id < idToData.size(); id++) {
            List<Double> studentData = new ArrayList<Double>(idToData.get(id));
            for (int j = 0; j < dimensions; j++) {
                if (studentData.get(j) != null) {
                    standardDeviation[j] = standardDeviation[j] + Math.pow(studentData.get(j)-averageArray[j], 2);
                } else {
                    numRows[j] = numRows[j] - 1;
                }
            }
        }
        for(int i = 0; i < dimensions; i++){
            standardDeviation[i] = Math.sqrt(standardDeviation[i]/numRows[i]);
        }
        return standardDeviation;
    }

    /*
    ** Computes the z value for the set of data
    ** Calls average() and standardDeviation()
    ** Fills clusterAssignments with initial clusters
    */
    public static void standardizeData(){
        Double[] sD = standardDeviation();
        Double[] averageArray = average();

        standardizedValues.clear();// = new HashMap<Integer, List<Double>>();
        clusterAssignments.clear();

        for (int id = 0; id < idToData.size(); id++) {
            List<Double> studentData = new ArrayList<Double>(idToData.get(id));
            List<Double> standarizedStudentData = new ArrayList<Double>();
            for (int j = 0; j < averageArray.length; j++) {
                if (studentData.get(j) != null) {
                    standarizedStudentData.add((studentData.get(j)-averageArray[j])/sD[j]);
                } else {
                    //Replace null with zero
                    standarizedStudentData.add(0.0);
                }
            }
            standardizedValues.put(id, standarizedStudentData);
            Set<Integer> temp = new HashSet<Integer>();
            temp.add(id);
            clusterAssignments.put(temp, standarizedStudentData);
        }

    }

    /*
    ** Calculates Euclidean Squared Distance for given list of doubles (vector)
    ** Gets called in initializePriorityQueue()
    */
    public static Double calcEucSquaredDist(List<Double> point1, List<Double> point2){
        Double eucSquaredDist = 0.0;
        for (int i = 0; i < point1.size(); i++){
            eucSquaredDist += Math.pow(point1.get(i)-point2.get(i), 2);
        }
        return eucSquaredDist;
    }

    /*
    ** Calculates distances for all cluster pairs and puts them into the priority queue
    ** Gets called in main function
    */
    public static void initializePriorityQueue(){

        distPriorityQueue = new PriorityQueue<ClusterPair>();
        standardizeData();
        for (int i = 0; i < standardizedValues.size(); i++){
            for (int j = i + 1; j < standardizedValues.size(); j++) {
                Double dist = calcEucSquaredDist(standardizedValues.get(i), standardizedValues.get(j));
                Set<Integer> cluster1 = new HashSet<Integer>();
                cluster1.add(i);
                Set<Integer> cluster2 = new HashSet<Integer>();
                cluster2.add(j);
                ClusterPair newClusterPair = new ClusterPair(cluster1, cluster2, dist);
                distPriorityQueue.add(newClusterPair);
            }
        }
    }

    /*
    ** Calculates distances between merged cluster and all other clusters in clusterAssignments
    ** Inserts new pair, distance object into distPriorityQueue
    ** Gets called in mergeClusters
    */
    public static void updatePriorityQueue(Set<Integer> thisCluster, List<Double> centroid){
        //calculate distance between centroid and all others clusters
        for(Set<Integer> otherCluster : clusterAssignments.keySet()){
            Double newDist = calcEucSquaredDist(centroid, clusterAssignments.get(otherCluster));
            ClusterPair newPair = new ClusterPair(thisCluster, otherCluster, newDist);
            distPriorityQueue.add(newPair);
        }
    }

    /*
    ** Takes merged cluster and calculates + returns centroid
    ** Inserts new pair, distance object into distPriorityQueue
    ** Gets called in mergeClusters
    */
    public static List<Double> calculateCentroid(Set<Integer> cluster1, Set<Integer> cluster2){
        Double[] resultArray = new Double[dimensions];

        //Initialize all values in arrays
        for (int d = 0; d < dimensions; d++){
            resultArray[d] = clusterAssignments.get(cluster1).get(d)*cluster1.size() + clusterAssignments.get(cluster2).get(d)*cluster2.size();
        }

        //Now resultArray contains sums of values from both clusters
        //Calculates average of all the values in cluster1 and cluster2
        for(int i = 0; i < dimensions; i++){
            resultArray[i] = resultArray[i]/(cluster1.size()+cluster2.size());//numRows[i];
        }
        return Arrays.asList(resultArray);
        
    }

    /*
    ** Merges clusters until we are left with k clusters
    */
    public static void mergeClusters(){
        while (clusterAssignments.size() > k) {

            //Look into priority queue to find shortest distance
            ClusterPair closestPair = new ClusterPair(distPriorityQueue.poll());
            Set<Integer> firstCluster = new HashSet<Integer>(closestPair.getCluster1());
            Set<Integer> secondCluster = new HashSet<Integer>(closestPair.getCluster2());

            //While the two sets are not both valid clusters and the pq is not empty
            while (!(clusterAssignments.containsKey(firstCluster) && clusterAssignments.containsKey(secondCluster)) && !distPriorityQueue.isEmpty()) {
                closestPair = distPriorityQueue.poll();
                firstCluster = closestPair.getCluster1();
                secondCluster = closestPair.getCluster2();
            }

            //Merge this pair and recalculate centroid
            List<Double> newCentroid = new ArrayList<Double>(calculateCentroid(firstCluster,secondCluster));
            Set<Integer> mergedCluster = new HashSet<Integer>();
            mergedCluster.addAll(firstCluster);
            mergedCluster.addAll(secondCluster);
            
            //Update clusterAssignments
            clusterAssignments.remove(firstCluster);
            clusterAssignments.remove(secondCluster);
            updatePriorityQueue(mergedCluster, newCentroid);
            clusterAssignments.put(mergedCluster, newCentroid);
        }
    }
    /*
    ** Calculates to total SSE for the current set of cluster centers
    */
    public static Double calcTotalSSE(){
        Double totalDist = 0.0;
        for(Set<Integer> cluster : clusterAssignments.keySet()){

            List<Double> centroid = clusterAssignments.get(cluster);

            for(Integer point : cluster){
                Double dist = calcEucSquaredDist(standardizedValues.get(point), centroid);
                totalDist = dist + totalDist;
            }
        }
        return totalDist;
    }

    public static HashMap<Set<Integer>, List<Double>> destandardizeData(){
        
        HashMap<Set<Integer>, List<Double>> destandardizedClusters = new HashMap<Set<Integer>, List<Double>>();
        for (Set<Integer> clusters : clusterAssignments.keySet()){
            Integer dataSize = (Integer)clusters.size();
            Double[] centroidArray = new Double[dimensions];
            //Initialize all values in array to 0
            for (int d = 0; d < dimensions; d++){
                centroidArray[d] = 0.0;
            }

            for (Integer id : clusters) {
                List<Double> studentData = new ArrayList<Double>(idToData.get(id));
                for (int j = 0; j < dimensions; j++) {
                    if (studentData.get(j) != null) {
                        centroidArray[j] = centroidArray[j] + studentData.get(j);
                    }
                }
            }
            //Now averageArray contains sums for all columns
            for(int i = 0; i < dimensions; i++){
                centroidArray[i] = centroidArray[i]/dataSize;
            }
            destandardizedClusters.put(clusters,Arrays.asList(centroidArray));
        }
        return destandardizedClusters;
    }

    public static void main(String[] args){
        readFile();
        for (int i = 1; i <=20; i++) {
            k = i;
            initializePriorityQueue(); 
            mergeClusters();
            HashMap<Set<Integer>, List<Double>> destandardizedClusters = destandardizeData();
            System.out.println("k is "+k);
            System.out.println("Total SSE: " +calcTotalSSE());
        }

    }
}